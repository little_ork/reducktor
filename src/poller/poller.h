#ifndef POLLER_LIB
#define POLLER_LIB

#include <sys/epoll.h>

typedef int (*pollerHandler)(void);

typedef struct pollerBuilder *      pollerBuilder;
typedef struct pollerInstance *     pollerInstance;

pollerBuilder       pollerBuilder_init      ();
void                pollerBuilder_drop      (pollerBuilder *pb);
void                pollerBuilder_addEvent  (pollerBuilder pb, int fd, unsigned int event_type, pollerHandler handler);

pollerInstance      pollerBuilder_build     (pollerBuilder *pb);
void                pollerInstance_drop     (pollerInstance *p);

int                 pollerInstance_poll     (pollerInstance p);

#endif