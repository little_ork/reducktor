#include "poller.h"

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

struct pbEventList {
    int fd;

    struct epoll_event event;
    struct pbEventList *next;
};

struct pollerBuilder {
    int         events_count;

    struct pbEventList *first;
    struct pbEventList *last;
};

struct pollerInstance {
    int         e_fd;

    int         events_count;
    struct epoll_event  *events;
};

pollerBuilder       pollerBuilder_init      () {
    pollerBuilder pb = malloc(sizeof(struct pollerBuilder));

    if(!pb) {
        perror("pollerBuilder_init");
        exit(EXIT_FAILURE);
    }

    pb->events_count = 0;
    pb->first = NULL;
    pb->last = NULL;

    return pb;
};

void                pollerBuilder_drop      (pollerBuilder *_pb) {
    pollerBuilder pb = *_pb;

    pb->last = NULL;

    while(pb->events_count) {

        void *t = pb->first;
        pb->first = pb->first->next;
        free(t);

        pb->events_count--;
    }
    
    free(pb);

    *_pb = NULL;
}

void                pollerBuilder_addEvent  (pollerBuilder pb, int fd, unsigned int event_type, pollerHandler handler) {
    pb->events_count++;

    if(!pb->last) {
        pb->first = malloc(sizeof(struct pbEventList));
        pb->last = pb->first;
    } else {
        pb->last->next = malloc(sizeof(struct pbEventList));
        pb->last = pb->last->next;
    }

    if(!pb->last) {
        perror("pollerBuilder_addEvent");
        exit(EXIT_FAILURE);
    }

    pb->last->fd = fd;
    pb->last->event = (struct epoll_event){.events = event_type, .data.ptr = handler};
    pb->last->next = NULL;

}

pollerInstance      pollerBuilder_build     (pollerBuilder *_pb) {

    pollerBuilder pb = *_pb;

    pollerInstance p = malloc(sizeof(struct pollerInstance));
    if(!p) goto error;

    p->events_count = pb->events_count;

    p->e_fd = epoll_create(69);
    if(p->e_fd == -1) goto error;

    p->events = calloc(p->events_count, sizeof(struct epoll_event));
    if(!p->events) goto error;

    for(int i = 0; i < p->events_count; i++) {
        p->events[i] = pb->first->event;

        if(epoll_ctl(p->e_fd, EPOLL_CTL_ADD, pb->first->fd, p->events +i) == -1)
            goto error;
        
        void *t = pb->first;
        pb->first = pb->first->next;

        free(t);
    }

    free(pb);
    *_pb = NULL;

    return p;

    error:
        perror("pollerBuilder_build");
        exit(EXIT_FAILURE);

        return NULL; // mb some compiler get it weird idk
}

void                pollerInstance_drop     (pollerInstance *_p) {
    pollerInstance p = *_p;

    free(p->events);
    free(p);

    *_p = NULL;
}

int                 pollerInstance_poll     (pollerInstance p) {

    int c = epoll_wait(p->e_fd, p->events, p->events_count, -1);

    if(c == -1) {
        perror("epoll_wait"); 
        exit(EXIT_FAILURE);
    }

    for(int i = 0, res; i < c; i++)
        if( (res = ((pollerHandler)p->events[i].data.ptr)()) ) return res;

    return 0;
}
