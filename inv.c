#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#include <signal.h>
#include <sys/ioctl.h>
#include <sys/signalfd.h>
#include <unistd.h>
#include <sys/epoll.h>
#include <fcntl.h>  
#include <termio.h>


#define cherr(str) do { perror(str); exit(EXIT_FAILURE); } while(0)


const uint32_t max_events = 10;


typedef struct {
    char *path;
    char *file_name;
    char *file_type;

    int descriptor;

    size_t fpos;
    size_t cpos;

    size_t sposs;
    size_t spose;

    size_t buffer_sizes;
    char *buffer;
} file; 

typedef struct {
    uint32_t files_c;
    file *files;


} Editor;

int addtoepoll(int fd, int e_fd) {

    struct epoll_event ev;

    ev.events = EPOLLIN;
    ev.data.fd = fd;

    if(epoll_ctl(e_fd, EPOLL_CTL_ADD, fd, &ev) == -1) 
        cherr("epoll_ctl");

    return 0;
}

int main ( void ) {

    struct epoll_event events[max_events];
    int e_fd, pfd;

    sigset_t mask;
    int sfd;

    

    sigemptyset(&mask);
    sigaddset(&mask, SIGWINCH);
    sigaddset(&mask, SIGINT);

    if(sigprocmask(SIG_BLOCK, &mask, NULL) == -1 ) 
        cherr("sigpromask");

    sfd = signalfd(-1, &mask, 0);
    if(sfd == -1) 
        cherr("signalfd");


    e_fd = epoll_create(69);
    if(e_fd == -1) 
        cherr("epoll_create");

    addtoepoll(STDIN_FILENO, e_fd);
    addtoepoll(sfd, e_fd);


    struct termios oldt, newt;

    tcgetattr( 0, &oldt );

    newt = oldt;
    newt.c_lflag &= ~( ICANON | ECHO );

    tcsetattr( 0, TCSANOW, &newt );

    setbuf(stdin, NULL);
    setbuf(stdout, NULL);


    for(;;) {

        pfd = epoll_wait(e_fd, events, max_events, -1);

        if(pfd == -1) cherr("epoll_wait");

        for(int n = 0; n < pfd; ++n)
            if(events[n].data.fd == STDIN_FILENO) {

                int c = getchar();

                //printf(" d %i d ", c );

                //putc(c, stdout);


                switch (c) {

                    // backspace
                    case 127: {
                        putchar('\b');
                        putchar(' ');
                        putchar('\b');
                    } break;

                    case ' '...'~': {
                        putc(c, stdout);
                    } break;

                    case '\e': {
                        //putc(' ', stdout);
                        if(getchar() == 91) switch (getchar()) {
                            case 51: {
                                if(getchar() == 126) putchar(' ') && putchar('\b');
                            } break;

                            case 68: {
                                putchar('\b');
                            } break;

                            case 67: {
                                putchar('\e[C');
                            } break;

                            default: {} break;
                        };
                            if(getchar() == 51)
                                if(getchar() == 126)
                                    putchar(' ') && putchar('\b');
                    } break;

                    default: {} break;
                } 

                

                // ...

            } else if (events[n].data.fd == sfd) {

                struct signalfd_siginfo sfd_si;

                if (read(sfd, &sfd_si, sizeof(sfd_si)) == -1) 
                    cherr("read " );

                switch (sfd_si.ssi_signo) {

                    case SIGINT: {

                        puts("\nescaping ...");

                        return EXIT_SUCCESS;

                    } break;

                    case SIGWINCH: {

                        struct winsize winsz;

                        ioctl(0, TIOCGWINSZ, &winsz);
                        printf("\nSIGWINCH raised, window size: %d rows / %d columns\n", winsz.ws_row, winsz.ws_col);

                    } break;

                    default: {} break;
                }
            }
        //fflush(stdout);
    }


    tcsetattr( 0, TCSANOW, &oldt );


    return EXIT_SUCCESS;
}